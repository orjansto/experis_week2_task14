# Experis_Week2_Task14

## Task 14: Two 3-digit palindrome

#### Group members: Kjetil Huy Tran, Ørjan Storås

The random task 14 which was released on a random time is Palindrome. Palindrome is a concept where a certain text can be reversed and produce the exact same as the original text. 

This project takes instead two 3-digits, and finds the largest palindrome made from the product of the two 3-digits. The result is 906609 produced by the product of the two 3-digits 993 and 913. 
