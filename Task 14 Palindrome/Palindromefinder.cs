﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_14_Palindrome
{
    class PalindromFinder
    {
        Palindrome prevPalindrome;

        public PalindromFinder()
        {
            prevPalindrome = new Palindrome(0,0,0);
        }

        public bool isPalindrome(int input)
        {
            string inputStringRev = new string("");
            string inputString = input.ToString();
            for (int i = inputString.Length-1; i >= 0 ; i--)
            {
                inputStringRev += inputString.Substring(i, 1);
            }
            if (inputString == inputStringRev)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkPalindrom(int i, int j)
        {
            int prod = i * j;
            return isPalindrome(prod);
        }
        private void checkIfLarger(Palindrome testPal)
        {
            if(testPal.Palindrom > prevPalindrome.Palindrom)
            {
                prevPalindrome = testPal;  
            }
        }
        public void highestPalindromeOfNumbersLessThan(int x, int y)
        {
            for (int i = x; i > (x/2); i--)
            {
                for (int j = y; j > (y/2); j--)
                {
                    if (checkPalindrom(i, j))
                    {
                        checkIfLarger(new Palindrome(i, j, i * j));
                    }
                }

            }
            prevPalindrome.PrintToConsole();
        }
    }
}
