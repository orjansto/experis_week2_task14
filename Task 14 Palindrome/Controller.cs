﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_14_Palindrome
{
    class Controller
    {
        public Controller()
        {
            int a;
            int b;
            string rerun;
            
            do
            {
                rerun = "FOOBAR";
                Console.WriteLine("Find the largest palindrome of a product of to numbers less than A and B.");

                #region Getting input
                do
                {
                    Console.WriteLine("Please enter number A:");
                    if (int.TryParse(Console.ReadLine(), out a))
                    {
                        rerun = "FOOBAR";
                        if (a > 9999)
                        {
                            Console.WriteLine("Number is too large.");
                            rerun = "SHTF";
                        }
                        else if (a < 1)
                        {
                            Console.WriteLine("Number is too small.");
                            rerun = "SHTF";
                        }
                    }
                    else
                    {
                        Console.WriteLine("Number not a legal Int");
                        rerun = "SHTF";
                    }
                    
                } while (rerun != "FOOBAR");

                do
                {
                    Console.WriteLine("Please enter number B:");
                    if (int.TryParse(Console.ReadLine(), out b))
                    {
                        rerun = "FOOBAR";
                    }
                    else
                    {
                        rerun = "SHTF";
                    }
                    if (b > 9999)
                    {
                        Console.WriteLine("Number is too large.");
                        rerun = "SHTF";
                    }else if(b < 1)
                    {
                        Console.WriteLine("Number is too small.");
                        rerun = "SHTF";
                    }
                } while (rerun != "FOOBAR");
                #endregion

                PalindromFinder findPal = new PalindromFinder();
                findPal.highestPalindromeOfNumbersLessThan(a, b);

                Console.WriteLine("Do you want to rerun program? y/n");
                do
                {
                    if (rerun != "FOOBAR")
                    {
                        Console.WriteLine("Enter y/n");
                    }
                    rerun = Console.ReadLine().ToLower();
                } while (rerun != "n" && rerun != "y");

            } while (rerun == "y");
        }
    }
}
