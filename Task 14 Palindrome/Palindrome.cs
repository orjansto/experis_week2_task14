﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_14_Palindrome
{
    class Palindrome
    {
        private int x;
        private int y;
        private int palindrom;

        public Palindrome(int x, int y, int palindrom)
        {
            this.x = x;
            this.y = y;
            this.palindrom = palindrom;
        }

        public int Palindrom { get => palindrom; }

        public void PrintToConsole()
        {
            Console.WriteLine($"The product of {x} and {y} result in the palindrom {palindrom}");
        }
    }
}
